# README #

### ¿Qué contiene el repositorio? ###

*Este repositorio contiene un trabajo práctico hecho para la materia Organización del Computador, de la Facultad de Ingeniería de la Universidad de Buenos Aires.
El mismo consiste en una variación del problema de la mochila (knapsack problem) implementado en assembler de IBM Mainframe 390.
Su segunda parte es una implementación de bubble sort en assembler de Intel 8086.

### ¿Cómo pruebo el código? ###

* Requerimientos para correr el archivo de IBM 390: bajar el emulador http://z390.org/ e instalarlo. Instructivos de uso en la página.
* También es recomendable utilizar el z390 Assembler Studio: http://sourceforge.net/projects/z390ide/

* Para el código en lenguaje assembler de Intel 8086 utilicé nasm: http://www.nasm.us/