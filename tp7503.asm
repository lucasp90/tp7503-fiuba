;******************************************************************************************************
; 75.03 - Organizacion del computador - Trabajo Practico Intel 8086
; Alumno: Perea, Lucas Eduardo Ramses - Padron: 95368
; Primer Cuatrimestre 2014
;
; Descripcion del programa: 
; Imprime por pantalla el contenido de un archivo binario que contiene hasta 30 bpfcs de 16 bits
; ordenado ascendentemente o descendentemente. Se ha solicitado que el ordenamiento sea por medio de
; la tecnica de burbujeo optimizado (el pseudocodigo especificado se encuentra en el enunciado)
;------------------------------------------------------------------------------------------------------
; observaciones: el enunciado solicitaba que se muestre el contenido, sin especificar formato. 
; Debido a la simplicidad para constatar que la informacion del archivo binario ha sido ordenada
; apropiadamente, decidi que el formato a mostrar sea hexadecimal. De esta manera, se puede comparar
; lo exhibido en pantalla con lo que puede verse en la mayoria de los editores hexadecimales.
; Para usar el programa, se debe procesar el archivo 'numeros' ubicado en el directorio del programa
; en la entrega doy varios archivos de prueba que muestran el comportamiento del programa en casos
; distintos: archivo vacio, con un elemento, con algunos elementos, con 30 elementos y con mas de 30.
; para probar alguno de ellos, debe renombrarse a 'numeros'.
;******************************************************************************************************
segment pila stack
resb 256

segment datos data
fileName	db	"numeros",0		;el nombre del archivo debe terminar con un 0 binario!!!!
fHandle		resw 1				;handle del archivo
cantobjs	resw 1				;contador cantidad de elementos del archivo
vector		times 30 resw 1		;vector de 30 words para guardar los bpfcs 16b maximos
buffer		resw 1
auxiliar	resb 1
opcion		resb 1				;guarda la opcion seleccionada por el usuario
modalidad	resb 1				;opcion validada y "upcaseada" para determinar el tipo
								;de ordenamiento
msjConsulta	db	"Por favor indique modo de ordenamiento$"
msjOpciones	db	"A - ascendente / D - descendente: $"
msjErrOpen	db	"Error en apertura. Compruebe que tiene el archivo 'numeros' en su directorio$"
msjErrRead	db	"Error en lectura$"
msjErrClose	db	"Error en cierre$"
msjCloseOk	db	"El archivo fue cerrado correctamente$"
msjOpcInval	db	"Opcion invalida. Intente nuevamente. $"
separaNum	db	"|$"
saltoLinea	db	13,10,"$"
variablei	dw	0
msjIntroTit	db	"		75.03 - Organizacion del Computador - Trabajo Practico$"
msjIntroNom	db	"			Perea, Lucas Eduardo Ramses - 95368		$"
msjIntroCua	db	"			     Primer Cuatrimestre de 2014		$"
msjIntroSep	db	"|..............................................................................|$"
msjContVec	db	"		A continuacion se muestra el contenido del archivo leido$"
msjContAsc	db	"  A continuacion se muestra el contenido del archivo ordenado ascendentemente$"
msjContDes	db	"  A continuacion se muestra el contenido del archivo ordenado descendentemente$"
msjArchGde	db	"El archivo contiene mas numeros de los que soporta el programa. Por favor intente con otro archivo.$"
msjArchVac	db	"El archivo no tiene numeros a procesar. Por favor utilice otro archivo.$"
msjUnElem	db	"El archivo solo contiene un numero a procesar. A continuacion se muestra el contenido: $"
msjFinPgrm	db	"		      		FIN DEL PROGRAMA$"

segment codigo code
..start:
	mov		ax,datos			;ds <-- dir del segmento de datos
	mov		ds,ax
	mov		ax,pila				;ss <-- dir del segmento de pila
	mov		ss,ax

;Mensajes de Presentacion
	call	mostrarIntro
	
;ABRE EL ARCHIVO PARA LECTURA
	mov		al,0		         ;al = tipo de acceso (en este caso, lectura)
	mov		dx,fileName	         ;dx = dir del nombre del archivo
	mov		ah,3dh		         ;ah = servicio para abrir archivo 
	int		21h
	jc		errOpen
	mov		[fHandle],ax		; en ax queda el handle del archivo

	mov		si,0				;inicializo si para desplazarme por el vector
								;en el que guardo la informacion del archivo
;LEE EL ARCHIVO (registro)
	mov		bx,[fHandle]		;bx = handle del archivo
otroReg:
	cmp		si,60				;comparo el registro  con 60 (cantidad maxima de bytes)
	jle		cargoReg			;si es menor, cargo otro registro en el vector
	call	closeFil
	call	errArchGrande		;aviso que el archivo tiene mas numeros de los esperados
								;y salgo del programa
cargoReg:
	mov		cx,2				;cx = cantidad de bytes a escribir
    lea		dx,[buffer] 		;dx = direccion del buffer
	mov		ah,3fh				;leo tantos bytes como especifique en cx	
	int		21h	
	jc		errRead				;para avisar error de lectura
	cmp		ax,0				;chequeo si estoy en el fin del archivo
	je		cierroArch			;si termine, cierro el archivo
	call	aLittleEndian		
	add		si,2				;si no termina el archivo, avanzo un byte para el offset	
 	jmp		otroReg				;y busco otro registro del archivo
cierroArch:
	call 	closeFil
	
;EN si TENGO #bytesArchivo!!	
setCantidad:
	mov		[cantobjs],si		;cargo la cantidad de bytes a procesar en un rotulo.
	cmp		si,0
	je		archVacio
	cmp		si,2
	je		soloUnElem
	call	introMostrar
	jmp		imprVecOrig
archVacio:
	call	errArchVacio
soloUnElem:
	call	errUnElem
	
imprVecOrig:	
	call	mostrarIzqDer
	mov		dx,msjIntroSep		;pongo una barra separadora
	mov		ah,9
	int		21h	
	call	saltoDeLinea		;y salto de linea otra vez
	
;ORDENA EL VECTOR POR BURBUJEO OPTIMIZADO
;a continuacion enuncio los operandos asociados a cada variable expuesta por la
;catedra en el pseudocodigo del enunciado correspondiente a este tp:
;variablei = i // si = j // dx = buffer (al momento de swapear) // vector = vector
;cantobjs = vector.length
bubble:
	mov		word[variablei],cx		;lo asigno para i
proximoi:
	mov		si,0					;contador para j
proximoj:
	cmp		si,word[variablei]		;comparo j con i
	jl		procelems				;si es menor, proceso dos elementos del vector
	add		word[variablei],2		;si no, corro dos bytes el indice i 
	mov		dx,word[variablei]		;dx = valor actual de i
	cmp		dx,word[cantobjs]		;comparo i con long vector
	je		consulta				;si es igual, el vector fue ordenado y salto a 
									;preguntar al usuario si quiere ord ascendente o 
									;descendente
	jmp		proximoi				;si no es igual, salto al proximo i 
procelems:
	mov		di,word[variablei]		;cargo di con el offset de i
	mov		dx,word[vector+di]		;cargo dx con v[j] para compararlos en la op sgte
	cmp		dx,word[vector+si] 		;comparo v[i] con v[j]
	jl		swapeo					;si v[i] es menor, los intercambio
	add		si,2					;si no, corro dos bytes el indice j
	jmp		proximoj				;y proceso el proximo v[j]
swapeo:
	mov		dx,word[vector+di]		;buffer = v[i]
	mov		ax,word[vector+si]		;buffer2 = v[j]
	mov		word[vector+di],ax		;v[i] = v[j] (buffer2)
	mov		word[vector+si],dx		;v[j] = v[i] (buffer)
	add		si,2					;corro dos bytes el indice j
	jmp		proximoj				;salto a procesar el proximo v[j]

consulta:
	mov		dx,msjConsulta			;cargo dx con el mensaje para el usuario
	mov		ah,9
	int		21h
	call	saltoDeLinea
	mov		dx,msjOpciones			;display de opciones
	mov		ah,9					
	int		21h
	call	saltoDeLinea
	
	mov		ah,1					;servicio de lectura de caracter por teclado
	int		21h 					;se va a imprimir el la opc elegida en pantalla
	mov		[opcion],al				;guardo en opcion el caracter ingresado por el usuario
	call	saltoDeLinea			;salto de linea

;comparo la opcion ingresada con los caracteres D, d, A, a. Si la opcion coincide con
;alguno de ellos, seteo la modalidad en la que se exhibe la informacion ordenada
;(ascendente o descendente), si no, pide otro ingreso por teclado	
	cmp		byte[opcion],"D"		
	je		setDescendente
	cmp		byte[opcion],"d"
	je		setDescendente
	cmp		byte[opcion],"A"
	je		setAscendente
	cmp		byte[opcion],"a"
	je		setAscendente
	mov		dx,msjOpcInval
	mov		ah,9
	int		21h
	call	saltoDeLinea
	jmp		consulta
setAscendente:
	mov		byte[modalidad],"A"
	jmp		imprContOrd
setDescendente:
	mov		byte[modalidad],"D"

;Impresion del contenido ordenado segun la modalidad seleccionada
imprContOrd:	
	cmp		byte[modalidad],"A"		;comparo la modalidad con la opcion "A"
	je		imprVecA				;si es igual, muestro en forma ascendente
	
imprVecD:							;si no, en forma descendente (si no tiene A, seguro tiene D)
	call	introMostrarDescendente
	call	mostrarDerIzq
	jmp		fin						; y termino
	
imprVecA:
	call	introMostrarAscendente
	call	mostrarIzqDer
	jmp		fin						; y termino

;RUTINA QUE MUESTRA MENSAJES INTRODUCTORIOS EN EL PROGRAMA
mostrarIntro:	
	mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
    mov		dx,msjIntroTit
	mov		ah,9
	int		21h
	call	saltoDeLinea
    mov		dx,msjIntroNom
	mov		ah,9
	int		21h
	call	saltoDeLinea
    mov		dx,msjIntroCua
	mov		ah,9
	int		21h
	call	saltoDeLinea
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
	ret
	;CIERRA EL ARCHIVO
closeFil:
	mov		bx,[fHandle]			;bx = handle del archivo
	mov		ah,3eh					;ah = servicio para cerrar archivo: 3eh
	int		21h
	jc		errClose				;Carry <> 0
	
;IMPRIMO MENSAJE DE FIN OK
    mov		dx,msjCloseOk
	mov		ah,9
	int		21h
	call	saltoDeLinea
	ret

;RUTINA PARA MOSTRAR MENSAJE PREVIO AL CONTENIDO DEL VECTOR
introMostrar:
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
	mov		dx,msjContVec
	mov		ah,9
	int		21h
	call	saltoDeLinea
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
	ret

;RUTINA PARA MOSTRAR MENSAJE PREVIO AL CONTENIDO DEL VECTOR ORDENADO ASCENDENTEMENTE
introMostrarAscendente:
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
	mov		dx,msjContAsc
	mov		ah,9
	int		21h
	call	saltoDeLinea
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
	ret

saltoDeLinea:
	mov		dx,saltoLinea
	mov		ah,9
	int		21h
	ret
	
;RUTINA PARA MOSTRAR MENSAJE PREVIO AL CONTENIDO DEL VECTOR ORDENADO ASCENDENTEMENTE
introMostrarDescendente:
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
	mov		dx,msjContDes
	mov		ah,9
	int		21h
	call	saltoDeLinea
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
	ret

;RUTINA PARA CONVERTIR A ASCII UN ELEMENTO UBICADO EN dl
convascii:
	cmp		dl,10
	jl		sumo30
	add		dl,55
	ret	
sumo30:
	add		dl,30h
	ret
	
;IMPRIME ERROR DE APERTURA DE ARCHIVO	
errOpen:
	mov		dx,msjErrOpen
	mov		ah,9
	int		21h
	call	saltoDeLinea
	jmp		fin

;IMPRIME ERROR DE LECTURA DE ARCHIVO
errRead:
	mov		dx,msjErrRead
	mov		ah,9
	int		21h

;IMPRIME ERROR DE CIERRE DE ARCHIVO
errClose:
	mov		dx,msjErrClose
	mov		ah,9
	int		21h
	jmp		fin

;IMPRIME ERROR: ARCHIVO SUPERA LOS 30 NUMEROS
errArchGrande:
	mov		dx,msjArchGde
	mov		ah,9
	int		21h
	call	saltoDeLinea
	jmp		fin

;IMPRIME ERROR: ARCHIVO SUPERA LOS 30 NUMEROS
errArchVacio:
	mov		dx,msjArchVac
	mov		ah,9
	int		21h
	call	saltoDeLinea
	jmp		fin
	
;IMPRIME ERROR: ARCHIVO SOLO TIENE UN NUMERO	
errUnElem:
	mov		dx,msjUnElem
	mov		ah,9
	int		21h
	mov		si,1					; offset actual respecto del vector
	mov		dx,0					; limpio dx
	lea		bx,[vector]				;bx <- direccion del vector
	call	mostrarNum
	call	saltoDeLinea
	jmp		fin

	;IMPRIME UN SEPARADOR DE ELEMENTOS	
separoNum:
    mov		dx,separoNum
	mov		ah,9
	int		21h
	ret

;MUESTRA EL PRIMER NIBBLE DE UN BYTE ALOCADO EN EL REGISTRO dl
mostrarPrimerNibble:
	shr		dl,4					;agarro el primer nibble
	call	convascii
	mov		ah,2					;Imprimo el elemento (de 'dl') en pantalla
	int		21h
	ret

;MUESTRA EL SEGUNDO NIBBLE DE UN BYTE ALOCADO EN EL REGISTRO dl
mostrarSegundoNibble:
	shl		dl,4
	shr		dl,4					;agarro el segundo nibble
	call	convascii
	mov		ah,2					;Imprimo el elemento (de 'dl') en pantalla
	int		21h
	ret

;RUTINA PARA PASAR UN NUMERO BPFCS DE 16 BITS AL FORMATO LITTLE ENDIAN
aLittleEndian:
	mov		dx,word[buffer]		;dx <- buffer
	mov		byte[auxiliar],dl
	mov		dl,dh
	mov		dh,byte[auxiliar]
	mov		word[vector+si],dx
	ret

;MUESTRA UN BYTE, RESTAURANDO EL NUMERO A FORMATO BIG ENDIAN	
mostrarByte:
	mov		dl,byte[bx+si]			;dl <- byte apuntado por bx+si
	call	mostrarPrimerNibble
	mov		dl,byte[bx+si]			;dl <- byte apuntado por bx+si
	call 	mostrarSegundoNibble
	ret

;MUESTRA UN NUMERO CONTENIDO EN EL VECTOR	
mostrarNum:
	call	mostrarByte
	sub		si,1					; me posiciono en el sgte byte del vector
	call	mostrarByte
	ret

;MUESTRA EL CONTENIDO DEL VECTOR DE IZQUIERDA A DERECHA
mostrarIzqDer:
	mov		si,1					; offset actual respecto del vector
	mov		dx,0					; limpio dx
	lea		bx,[vector]				;bx <- direccion del vector
imprElem:
	call	mostrarNum
	add		si,3
	cmp		si,[cantobjs]			; comparo si el offset coincide con la cantidad de bytes del vector
	jle		imprElem				; si es menor, imprimo otro elemento
	call	saltoDeLinea			; si no, salto de linea
	ret

;MUESTRA EL CONTENIDO DEL VECTOR DE DERECHA A IZQUIERDA
mostrarDerIzq:
	mov		si,[cantobjs]			;si <- offset respecto del vector
	sub		si,1					;resto 1 para ubicarme la parte baja (LE) del ultimo numero del vector
	mov		dx,0					;limpio dx
	lea		bx,[vector]				;bx <- direccion del vector	
imprElemDerIzq:
	call	mostrarNum
	sub		si,1					; me posiciono en el sgte elemento del vector
	cmp		si,0					; comparo el offset con cero
	jge		imprElemDerIzq			; si es mayor o igual, imprimo el proximo elemento
	call	saltoDeLinea
	ret
	
;FIN DE PROGRAMA	
fin:
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h	
	call	saltoDeLinea
    mov		dx,msjFinPgrm
	mov		ah,9
	int		21h
	call	saltoDeLinea
    mov		dx,msjIntroSep
	mov		ah,9
	int		21h
	mov		ax,4c00h  				; retornar al DOS
	int		21h
	
